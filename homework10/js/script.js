"use strict";
const ul = document.querySelector(".tabs");
const tabsContent = document.querySelectorAll(".tabs-content li");
// console.log(tabsContent);
ul.addEventListener("click", function (event) {
  if (event.target.tagName === "LI") {
    const active = document.querySelector(".active");
    active.classList.remove("active");
    event.target.classList.add("active");

    const atr = event.target.getAttribute("data-tab");

    tabsContent.forEach((item) => {
      let tabs2 = item.getAttribute("data-tab");
      // console.log(tabs2);
      if (tabs2 === atr) {
        item.classList.add("show");
      } else {
        item.classList.remove("show");
      }
    });
  }
});
