"use strict";

function calcResult() {
  let numFirst = +prompt("Введите первое число");
  let numSecond = +prompt("Введите второе число");
  let operator = prompt("Введите знак операции");
  switch (operator) {
    case "+":
      return numFirst + numSecond;
    case "-":
      return numFirst - numSecond;
    case "*":
      return numFirst * numSecond;
    case "/":
      return numFirst / numSecond;
  }
}
alert(calcResult());

// Option two
// let numFirst = +prompt('Введите первое число');
// let numSecond = +prompt('Введите второе число');
// let operator = prompt('Введите знак операции');

// function calcResult(numFirst, numSecond, operator) {
//   return eval(`${numFirst}` + operator + `${numSecond}`);
// }
// // alert(calcResult(numFirst, numSecond, operator));
// console.log(calcResult(numFirst, numSecond, operator));
