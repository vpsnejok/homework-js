"use strict";
const slides = document.querySelectorAll(".image-to-show");
let sliderCount = 1;

function slider() {
  // Перебор и удаление класса
  slides.forEach((img) => {
    img.classList.remove("show");
  });
  slides[sliderCount].classList.add("show");

  if (sliderCount === slides.length - 1) {
    sliderCount = 0;
  } else {
    sliderCount++;
  }
}

let intervalid = setInterval(slider, 1000);
// console.log(intervalid);

const start = document.querySelector(".start");
const stop = document.querySelector(".stop");

start.addEventListener("click", () => {
  clearInterval(intervalid);
  intervalid = setInterval(slider, 1000);
});
stop.addEventListener("click", () => {
  clearInterval(intervalid);
});
