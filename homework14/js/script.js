"use strict";
const btn = document.querySelector(".btn");

btn.addEventListener("click", function () {
  const element = document.body;
  element.classList.toggle("dark-mode");
})