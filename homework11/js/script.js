"use strick";
const form = document.querySelector(".password-form");
form.addEventListener("click", function ({ target }) {
  if (target.classList.contains("fas")) {
    target.classList.toggle("fa-eye-slash");
    const label = target.closest("label");

    const input = label.querySelector("input");
    if (input.type === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  }
});

form.addEventListener("submit", function (event) {
  event.preventDefault();

  //   form.querySelector('p')?.remove()

  if (form.querySelector("p")) {
    form.querySelector("p").remove();
  }

  const button = document.querySelector("button");

  const p = document.createElement("p");
  p.textContent = "Нужно ввести одинаковые значения";
  p.style.color = "red";

  if (form.elements.firstpass.value === form.elements.secondpass.value) {
    alert("You are welcome");
  } else {
    button.insertAdjacentElement("beforebegin", p);
  }
});
