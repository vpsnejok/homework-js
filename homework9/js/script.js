"use strick";

// function showCity (arr, parentSelector = default){
// const parent = document.querySelector(parentSelector);
// arr.forEach(item => {
//     const li = document.createElement("li");
//     li.textContent = item;
//     parent.append(li);
// });
// };
// showCity(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],".list1")

function showCity(arr, parent = document.body) {
  const ul = document.createElement("ul");
  const div = document.querySelector(parent);

  let cities = arr.map((item) => {
    const li = document.createElement("li");
    li.textContent = item;
    return li;
  });
  for (let i = 0; i < cities.length; i++) {
    ul.append(cities[i]);
  }
  div.append(ul);
}

showCity(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], "span");
