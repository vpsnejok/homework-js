"use strict";

const ul = document.querySelector(".services-list");
const tabsContent = document.querySelectorAll(".service-item-secont");

ul.addEventListener("click", function ({ target }) {
  if (target.tagName === "UL") {
    return;
  }
  if (target.tagName === "LI") {
    const active = document.querySelector(".active");
    active.classList.remove("active");
    target.classList.add("active");
  }
  const dataAtr = target.getAttribute("data-id");

  tabsContent.forEach((item) => {
    let tabs2 = item.getAttribute("data-id");
    if (tabs2 === dataAtr) {
      item.classList.add("show");
    } else {
      item.classList.remove("show");
    }
  });
});
// =======================================================
const workList = document.querySelector(".work-list");
const workItems = document.querySelectorAll(".work-item-grid");

workList.addEventListener("click", function ({ target }) {
  // console.log(target);
  if (target.tagName === "UL") {
    return;
  }
  if (target.tagName === "LI") {
    const active = document.querySelector(".work-item-active");
    active.classList.remove("work-item-active");
    target.classList.add("work-item-active");
  }
  const test = target.getAttribute("data-id");

  workItems.forEach((item) => {
    let tabs2 = item.getAttribute("data-id");
    if (tabs2 === test) {
      item.classList.remove("hide");
    } else {
      item.classList.add("hide");
    }
    if (target.getAttribute("data-id") === "all") {
      item.classList.remove("hide");
    }
  });
});

const images = [
  "/img/wordpress/wordpress1.jpg",
  "/img/wordpress/wordpress2.jpg",
  "/img/wordpress/wordpress3.jpg",
  "/img/wordpress/wordpress4.jpg",
  "/img/wordpress/wordpress5.jpg",
  "/img/wordpress/wordpress6.jpg",
  "/img/wordpress/wordpress7.jpg",
  "/img/wordpress/wordpress8.jpg",
  "/img/wordpress/wordpress9.jpg",
];

const btn = document.querySelector(".load");

btn.addEventListener("click", function ({ target }) {
  // console.log(target);
  images.forEach((src) => {
    const img = document.createElement("img");
    const ul = document.querySelector(".work-list-grid");
    const li = document.createElement("li");

    li.classList.add("work-item-grid");
    img.src = src;

    li.append(img);
    ul.append(li);
  });
  btn.remove();
});

// =============================================================

const photolover = document.querySelector(".client-list-img");
const photoBig = document.querySelectorAll(".client-item");

photolover.addEventListener("click", function ({ target }) {
  if (target.tagName === "UL") {
    return;
  }
  if (target.tagName === "IMG") {
    const parent = target.parentElement;
    const test = document.querySelector(".text-test");
    test.classList.remove("text-test");
    parent.classList.add("text-test");
  }

  const atrlowerphoto = target.getAttribute("data-img");

  photoBig.forEach((item) => {
    let test = item.getAttribute("data-img");

    if (test === atrlowerphoto) {
      item.classList.add("client-show");
    } else {
      item.classList.remove("client-show");
    }
  });
});
//! ====================================================

const prev = document.querySelector(".client-btn-left");
const next = document.querySelector(".client-btn-right");
const imageslide = document.querySelectorAll(".client-list-img li");
const slides = document.querySelectorAll(".client-item");

let index = 0;

prev.addEventListener("click", () => {
  nextImage("prev");
});

next.addEventListener("click", () => {
  nextImage("next");
});

function nextImage(direction) {
  if (direction === "next") {
    index++;
    if (index === imageslide.length) {
      index = 0;
    }
  } else {
    if (index === 0) {
      index = imageslide.length - 1;
    } else {
      index--;
    }
  }

  for (let i = 0; i < imageslide.length; i++) {
    imageslide[i].classList.remove("text-test");
    slides[i].classList.remove("client-show");
  }
  imageslide[index].classList.add("text-test");
  slides[index].classList.add("client-show");
}
