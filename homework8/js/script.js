"use strick";

const paracolor = document.querySelectorAll("p");
console.log(paracolor);
paracolor.forEach((item) => {
  item.style.backgroundColor = "#ff0000";
});

const optionsList = document.querySelector("#optionsList");
console.log(optionsList);

const list = optionsList.parentElement;
console.log(list);

const listchild = optionsList.childNodes;
console.log(listchild);
console.log([...listchild]);
console.log(Array.from(listchild));

listchild.forEach((item) => {
  console.log(item);
});

const testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = "This is a paragraph";

const classElem = document.querySelectorAll(".main-header li");
classElem.forEach((item) => {
  item.classList.add("nav-item");
});

const sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach((item) => {
  item.classList.remove("section-title");
});
console.log(sectionTitle);
