"use strict";

function createNewUser() {
  let fname = prompt("Enter your Firstname");
  let lname = prompt("Enter your Lastname");

  const newUser = {
    firstName: fname,
    lastName: lname,

    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };
  return newUser;
}

// console.log(createNewUser().getLogin());
const user1 = createNewUser();
const user2 = createNewUser();
console.log(user1);
console.log(user2);
