"use strick";
document.addEventListener("keydown", function (event) {
  console.log(event.code);
  const btns = document.querySelectorAll(".btn-wrapper button");
  btns.forEach((item) => {
    item.classList.remove("blue");

    const atribut = item.getAttribute("data-id");
    if (event.code === atribut) {
      item.classList.add("blue");
    }
  });
});
