"use strict";

function createNewUser() {
  //   let fname = prompt("Enter your Firstname");
  //   let lname = prompt("Enter your Lastname");
  //   let date = prompt("Enter your Birthday", "dd.mm.yyyy");

  const newUser = {
    _firstName: prompt("Enter your Firstname"),
    set firstName(value) {
      this._firstName = value;
    },
    get firstName() {
      return this._firstName;
    },

    lastName: prompt("Enter your Lastname"),
    date: prompt("Enter your Birthday", "dd.mm.yyyy"),
    // birthday: new Date(date),

    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    // getAge(birthday) {
    //   let now = Date.now() - this.birthday.getTime();
    //   let ageDate = new Date(now);
    //   return Math.abs(ageDate.getUTCFullYear() - 1970);
    // },

    getAge: function () {
      let currentDate = new Date();
      let userBirthdayDay = this.date.split(".")[0];
      let userBirthdayMonth = this.date.split(".")[1];
      let userBirthdayYear = this.date.split(".")[2];
      let userBirthdayDate = new Date(
        userBirthdayYear,
        userBirthdayMonth - 1,
        userBirthdayDay
      );
      let userAge = Math.floor(
        (currentDate - userBirthdayDate) / (1000 * 60 * 60 * 24 * 365)
      );
      return userAge;
    },

    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday
      );
    },
  };
  return newUser;
}

const user1 = createNewUser();
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());

// const test = {
//     firstnumber: 5,

// }

// const arr1 = [1, 2, 3]
// const arr2 = arr1;
// arr2[0] = 'sdfsdf';
// console.log(arr1);

// console.log({a: 1} === {a: 1});
// console.log(arr1 === arr2);
